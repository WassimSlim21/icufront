import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ProjectService } from 'src/app/core/services/project.service';
import { Project } from 'src/app/core/models/project.module';
import { User } from 'src/app/core/models/user/user.module';
import { UserService } from 'src/app/core/services/user.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { exists } from 'fs';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { ProjectNotifyChangeService } from 'src/app/shared/services/project-notify-change.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.css']
})
export class ViewProjectComponent implements OnInit {
  private project: any;
  private user: User;
  public like: boolean = undefined;
  public favorites;
  closeResult: string;
  public progress: number = undefined;
  public contributionForm: FormGroup;

  currentFileUpload: any;
  timeStamp: any;
  selectedFiles: any;

  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private userService: UserService,
    private projectNotifyChangeService: ProjectNotifyChangeService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService:ToastrService
  ) {
    this.contributionForm = formBuilder.group({
      contribution: new FormControl('', [Validators.minLength(10)])
    });
  }
  addArchived(){
    this.projectService.archiveProject(this.project.id).subscribe(res=>{
      this.toastrService.success("Project has been archived")
      this.router.navigateByUrl('/')
    })
  }
  ngOnInit() {
    this.chargeProject();
    this.projectService.getFavorites().subscribe(value => {
      this.favorites = value.data;
    });
    this.projectNotifyChangeService.projectAnnounced.subscribe(res => {
      if (res) {
        this.projectService
          .getProjectById(this.project.id)
          .subscribe(
            value => (this.project = value['data']),
            error => {
              this.router.navigateByUrl('/')
              this.toastrService.error("404 Not Found");
            }
            );
      }
    });
  }

  public chargeProject() {
    const id = this.route.snapshot.params.id;
    this.projectService.getProjectById(id).subscribe((data: Project) => {
      this.project = data['data'];
      console.dir(this.project);
      this.userService.getUser().subscribe(value => {
        this.user = value['data'];
        this.like = this.isLiked();
      });
    },
    error => {
      this.router.navigateByUrl('/')
      this.toastrService.error("404 Not Found");
    });
  }

  public liking() {
    const id = this.route.snapshot.params.id;

    if (this.like == true) {
      this.projectService.setDislike(id, 'project').subscribe(
        data => {
          this.project.likes = data['data'];
          console.log(this.project.likes);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.projectService.setLike(id, 'project').subscribe(
        data => {
          this.project.likes = data['data'];
          console.log(this.project.likes);
        },
        error => {
          console.log(error);
        }
      );
    }
    this.like = !this.like;
  }

  public isLiked(): boolean {
    const id = this.route.snapshot.params.id;
    if (this.project != undefined) {
      const likes = this.project.likes;
      for (const like of likes) {
        if (like.user.id == this.user.id) {
          return true;
        }
      }
    }
    return false;
  }

  public isFavorites(): boolean {
    if (this.project != undefined && this.favorites != undefined) {
      for (const project of this.favorites) {
        if (project.id == this.project.id) {
          return true;
        }
      }
    }
    return false;
  }

  public setFavorites() {
    if (this.project.id != undefined) {
      if (this.isFavorites()) {
        this.projectService.removeFromFavorites(this.project.id).subscribe(
          data => {
            this.projectService.getFavorites().subscribe(value => {
              this.favorites = value['data'];
            });
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.projectService.addFavorites(this.project.id).subscribe(
          data => {
            this.projectService.getFavorites().subscribe(value => {
              this.favorites = value['data'];
            });
          },
          error => {
            console.log(error);
          }
        );
      }
    }
  }

  onSelectedFile(event) {
    this.selectedFiles = event.target.files[0];
  }
  public contribute() {
    if (this.project.mainContribution.type != 'media') {
      this.progress = 0;
      this.currentFileUpload = this.selectedFiles;
      this.projectService
        .contribute(this.currentFileUpload, this.project.id)
        .subscribe(
          event => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round((100 * event.loaded) / event.total);
              this.chargeProject();
            } else if (event instanceof HttpResponse) {
              this.timeStamp = Date.now();
            }
            this.currentFileUpload=null;
            this.contributionForm.reset();
          },
          error => {
            this.toastrService.error(error.message);
          }
        );
    } else {
      this.projectService
        .contribute(this.contributionForm.value.contribution, this.project.id)
        .subscribe(
          event => {
            console.log(this.contributionForm.value);
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round((100 * event.loaded) / event.total);
              this.chargeProject();
            } else if (event instanceof HttpResponse) {
              this.timeStamp = Date.now();
            }
            this.contributionForm.reset();
          },
          error => {
            this.toastrService.error(error.message);
          }
        );
    }
  }

  public allowedToContribute(choice: number): boolean {
    if (this.project) {
      if (choice == 0) {
        return (
          !this.isOwner() &&
          this.project.mainContribution.type != "media" &&
          this.project.selectedContribution == null
        );
      }
      else if (choice == 1) {
        return this.project.mainContribution.type != "media";
 }
      else if (choice == 11) {
        return !this.isOwner() && this.project.selectedContribution == null;
 }
      else if (choice == 12) {
        return !this.isOwner() && this.project.mainContribution.type == "media";
 }
    }
    return false;
  }

  public isOwner(): boolean {
    if (this.project && this.user) { return this.project.user.id == this.user.id; }
    return false;
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'modal' }).result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
