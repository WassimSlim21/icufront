import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewContributionComponent } from './view-contribution.component';

describe('ViewContributionComponent', () => {
  let component: ViewContributionComponent;
  let fixture: ComponentFixture<ViewContributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewContributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewContributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
