import { Component, OnInit, Input } from "@angular/core";
import { ProjectService } from "src/app/core/services/project.service";
import { ActivatedRoute } from "@angular/router";
import { User } from "src/app/core/models/user/user.module";
import { UserService } from "src/app/core/services/user.service";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { ProjectNotifyChangeService } from 'src/app/shared/services/project-notify-change.service';
import { Contribution } from "src/app/core/models/contribution.module";
@Component({
  selector: "app-view-contribution",
  templateUrl: "./view-contribution.component.html",
  styleUrls: ["./view-contribution.component.css"]
})
export class ViewContributionComponent implements OnInit {
  @Input()
  contributions;
  @Input()
  project;
  public like: boolean = undefined;
  public user: User;
  public selectedContributionLikes;
  public srcToShow: string = undefined;
  public verifOwner: boolean = false;
  public isChoosed: boolean = false;
  public isLike: boolean = false;
  public selectedContribution;

  private closeResult;
  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private userService: UserService,
    private projectNotifyChangeService: ProjectNotifyChangeService,
  ) {
    console.log(this.project)

  }

  ngOnInit() {
    this.userService.getUser().subscribe(value => {
      this.user = value["data"];
      this.projectService.getProjectById(this.project).subscribe(value => {
        let p = value["data"];
        this.verifOwner = (p.user.id == this.user.id);
      }, err => console.log(err));
    });
    this.projectNotifyChangeService.projectAnnounced.subscribe(res => {
      if (res)
        this.projectService.getProjectById(this.project).subscribe(value => {
          this.project = value["data"];
        }, err => console.log(err));
    });



  }

  public showIt(path: string) {
    if (this.srcToShow == undefined)
      this.srcToShow = path;
    else
      this.srcToShow = undefined;
  }


  public chooseContribution(cId: number) {
    let c = new Contribution(this.project, cId);
    console.dir(c);
    this.projectService.chooseContribution(c).subscribe(value => {
      this.projectNotifyChangeService.announceChange();
    }, err => { });
  }

  public liking(id: number) {
    if (this.isLiked(id) == true) {
      this.projectService.setDislike(id, "contribution").subscribe(
        data => {
          for (let c of this.contributions) {
            if (c.id == id) {
              c.likes = data["data"];
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.projectService.setLike(id, "contribution").subscribe(
        data => {
          for (let c of this.contributions) {
            if (c.id == id) {
              c.likes = data["data"];
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    }
    this.like = !this.like;
  }

  public isLiked(id): boolean {
    if (this.contributions != undefined) {
      for (let c of this.contributions) {
        if (c.id == id) {
          for (let like of c.likes) {
            if (this.user != undefined && like.user.id == this.user.id) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  public selectedLikes() {
    if (this.contributions != undefined) {
      for (let c of this.contributions) {
        if (c.id == this.selectedContributionLikes)
          return c.likes;
      }
    }
    return [];
  }

  public nbrLikes(id): number {
    if (this.contributions != undefined) {
      for (let c of this.contributions) {
        if (c.id == id) {
          return c.likes.length;
        }
      }
    }
    return 0;
  }



  public open(content, id) {
    this.isLike = true;
    this.selectedContributionLikes = id;
    /* if (this.selectedLikes)
       this.selectedLikes(id);*/

    this.modalService.open(content, { windowClass: "modal" }).result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  public openValidation(content, contribution) {
    this.isChoosed = true;
    this.selectedContribution = contribution;
    console.log(contribution)
    this.modalService.open(content, { windowClass: "modal" }).result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    this.isChoosed = false;
    this.isLike = false;
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
