import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { FormGroup, FormBuilder, Validators, FormControl, Form } from '@angular/forms';
import { NewPostComponent, DialogData } from 'src/app/shared/new-post/new-post.component';
import { ToastrService } from 'ngx-toastr';
import { Observable, from } from 'rxjs';
import { DragDropDirective } from './../../directives/drag-drop.directive'
import { ProjectService } from 'src/app/core/services/project.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {
  stepper: MatStepper;
  files: any = [];
  levels: any = [];
  plaforms: any = [];
  isLinear = false;
  myForm: FormGroup;
  isHovering: boolean;
  percent$: Observable<number>;
  upFile: File;
  fd = new FormData();
  constructor(public dialogRef: MatDialogRef<NewPostComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _formBuilder: FormBuilder,
    private ps: ProjectService,
    private toast: ToastrService) {


    this.myForm = this._formBuilder.group({
      title: ['', Validators.required],
      discription: ['', Validators.required],
      type: [, Validators.required],
      copy: [null,],
      media: [null,],
      draft: [0,],
      level: ['',],
      minAge: [0,],
      maxAge: [100,],
      finished: [0,],
      targetPlatform: ['',],
      sex: ['',],
      targetCriteria: ['',],


    });
  }

  get title() {
    return this.myForm.get('title');
  }

  get discription() {
    return this.myForm.get('discription');
  }

  get type() {
    return this.myForm.get('type');
  }

  get content() {
    return this.myForm.get('content');
  }

  ngOnInit() {

    this.myForm.valueChanges.subscribe(value => {
      console.log(value);
    });

    this.ps.getLevels().subscribe((res) => {

      this.levels = res;
      console.log("levels", this.levels);

    }, (err) => {
      this.toast.error(err);
      console.log(err);

    })

    this.ps.getPlatforms().subscribe((res) => {

      this.plaforms = res;
      console.log("platforms", this.plaforms);

    }, (err) => {
      this.toast.error(err);
      console.log(err);

    });



  }




  uploadFile(event) {
    this.upFile = event[0];
    console.log("component upload file" + event);
    console.dir(event[0]);
    this.upFile = event[0]
    this.files.push(event[0].name);
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }


  onNoClick(): void {


    this.dialogRef.close();
  }

  submit() {
    let data = this.myForm.value;

    this.fd.append("title", data.title);
    this.fd.append("description", data.discription);
    this.fd.append("level", data.level);
    this.fd.append("targetPlatform", "1");
    this.fd.append("minAge", data.minAge);
    this.fd.append("maxAge", data.maxAge);
    this.fd.append("sex", data.sex);
    this.fd.append("targetCriteria", data.targetCriteria);
    this.fd.append("draft", data.draft);
    this.fd.append("finished", JSON.stringify(1));


    if (this.myForm.value.type == "Content") {

      this.fd.append("copy", data.copy);



      this.ps.publishMedia(this.fd).subscribe((res) => {
        this.toast.success("published");
        this.dialogRef.close();

      }, (err) => {
        console.dir(err)
        this.toast.error("error" + err)
      })
    }



    else
      if (this.myForm.value.type == "Media") {

        this.fd.append("media", this.upFile);


        this.ps.publishMedia(this.fd).subscribe((res) => {
          this.toast.success("published");
          this.dialogRef.close();

        }, (err) => {
          console.dir(err)
          this.toast.error("error" + err)
        })



      }
      else {
        this.fd.append("media", this.upFile);
        this.fd.append("copy", data.copy);

        this.ps.publishMedia(this.fd).subscribe((res) => {
          this.toast.success("published");
          this.dialogRef.close();

        }, (err) => {
          console.dir(err)
          this.toast.error("error" + err)
        })



      }
  }

  isValid1()
  {
    let formm=this.myForm.value;

    if (formm.title.trim()=="" || formm.discription.trim()=="" || formm.type ==null)
    {
      return true;
    }

    return false;

  }

  isValid2()
  {
    let formm=this.myForm.value;
    return (formm.type=="Content" && formm.copy!=null) || (formm.type=="Media" && formm.media!=null) || (formm.type=="Both" && formm.copy!=null && formm.media!=null);

  }














}
