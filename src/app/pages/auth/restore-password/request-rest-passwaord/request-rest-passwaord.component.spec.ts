import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestRestPasswaordComponent } from './request-rest-passwaord.component';

describe('RequestRestPasswaordComponent', () => {
  let component: RequestRestPasswaordComponent;
  let fixture: ComponentFixture<RequestRestPasswaordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestRestPasswaordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestRestPasswaordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
