import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angular-6-social-login';
import { AuthhService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-request-rest-passwaord',
  templateUrl: './request-rest-passwaord.component.html',
  styleUrls: ['./request-rest-passwaord.component.css']
})
export class RequestRestPasswaordComponent implements OnInit {


  constructor(private authService:AuthhService,private tosterService:ToastrService) { }

  ngOnInit() {
  }

  requestNewPassWord(formBody){

    this.authService.requestNewPassWord(formBody.email).subscribe(
      res=>{
         this.tosterService.success('We have e-mailed your password reset link!');
      },
      err=>{
        this.tosterService.error('We cant find a user with that e-mail address.');
      }
    )

  }


}
