import { Component, OnInit } from '@angular/core';
import { AuthhService } from 'src/app/core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.css']
})
export class RestorePasswordComponent implements OnInit {

  passwordRestToken:string;
  constructor(private authhService:AuthhService,private tosterService:ToastrService,private route:ActivatedRoute,private router:Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.passwordRestToken = params.get("passwordRestToken")
    });

    this.authhService.checkPasswordrequest(this.passwordRestToken).subscribe(
      (res)=>{},
      (err)=>{
          this.tosterService.error("This password reset token is invalid.");
          this.router.navigateByUrl('/');
      }
    )
  }

  changePassword(body){

    this.authhService.sendNewPassWord(body.password,this.passwordRestToken).subscribe((res) => {
     this.tosterService.success("Password reset successfully");
      this.router.navigateByUrl('/');
    }, (err) =>{})

  }

}
