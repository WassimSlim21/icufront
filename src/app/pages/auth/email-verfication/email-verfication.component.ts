import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthhService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-verfication',
  templateUrl: './email-verfication.component.html',
  styleUrls: ['./email-verfication.component.scss']
})
export class EmailVerficationComponent implements OnInit {

  constructor(private router:Router,private route: ActivatedRoute,private authhservice:AuthhService,private toasterService:ToastrService) { }
  emailToken:String;

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.emailToken = params.get("emailToken")
    })

    this.authhservice.emailVerfication(this.emailToken).subscribe(
      res=>{
        this.toasterService.success("account activated successfully")
        this.authhservice.saveToken(res.token);
        this.router.navigateByUrl("/user");
      },
      err=>{
        this.toasterService.error("This activation token is invalid.")
        this.router.navigateByUrl('');
      }
      )


  }

}
