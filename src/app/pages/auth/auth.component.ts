import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { User } from "src/app/core/models/user/user.module";
import { AuthhService } from "src/app/core/services/auth.service";


import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from "angular-6-social-login";
import { ToastrService } from 'ngx-toastr';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  encapsulation: ViewEncapsulation.Native

})
export class AuthComponent implements OnInit {
  signUpform: FormGroup;
  showSignUp: boolean = false;


  constructor(
    private socialAuthService: AuthService,
    private signForm: FormBuilder,
    private title: Title,
    private router: Router,
    private authService: AuthhService,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private ngProgress: NgProgress
  ) {
    this.signUpform = signForm.group({
      username: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),

      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  get username() {
    return this.signUpform.get("username");
  }

  get email() {
    return this.signUpform.get("email");
  }

  get password() {
    return this.signUpform.get("password");
  }

  ngOnInit() {
    this.title.setTitle("Sign Up");
  }

  onLogin(user) {
    this.authService.login(user).subscribe(
      resp => {
        console.log(resp);
        let jwtToken = resp.body["Token"];
        this.authService.saveToken(jwtToken);
        this.router.navigateByUrl("/user");

      },
      error => {
        this.toastrService.error(error.error.error)
      }
    );
  }

  register() {
    let data = this.signUpform.value;
    const user = new User(data.username, data.email, data.password);

    this.authService.register(user).subscribe(
      res => {
        this.toastrService.success("account created successfully");
        this.toastrService.warning("check your email to activate your account");
        this.showSignUp = !this.showSignUp;
      },
      err => {
        err.error.forEach(element => {
          this.toastrService.error(element)
        });
      }
    );
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }console.log("sdfsdf")

    this.socialAuthService.signIn(socialPlatformProvider).then(userData => {
      console.log(userData.token);
      this.authService.getSocialInfo().subscribe(rez=>{
          this.authService.socialSignIn(userData.token, socialPlatform,rez[0]).subscribe(
            res => {
              localStorage.token = res.access_token;
              this.router.navigateByUrl("/user");
            },
            err => {
              console.log(err);
            }
          );
      });
    },
    err=>console.log(err));
  }
}
