import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/core/services/user.service";
import { ToastrService } from "ngx-toastr";
import { User } from "src/app/core/models/user/user.module";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { HttpEventType, HttpResponse } from "@angular/common/http";
import { UserNotifyChangeService } from "src/app/shared/services/user-notify-change.service";
import { AuthhService } from "src/app/core/services/auth.service";
@Component({
  selector: "app-user",
  templateUrl: "user.component.html",
  styleUrls: ["user.component.css"]
})
export class UserComponent implements OnInit {
  public user;
  public update: boolean = false;
  public updatePhoto: boolean = false;
  private selectedFiles = null;
  public progress: number = undefined;
  private currentFileUpload;
  private timeStamp: number;
  public updateForm: FormGroup;
  public isAuth: boolean = false;
  public selectedView: boolean = false;
  public projects = [];

  constructor(
    private authService: AuthhService,
    private userService: UserService,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userNotifyChangeService: UserNotifyChangeService
  ) {
    this.updateForm = formBuilder.group({
      username: new FormControl("", [Validators.minLength(2)]),
      email: new FormControl("", [
        Validators.required,
        Validators.email,
        Validators.minLength(2)
      ]),
      firstName: new FormControl("", [Validators.minLength(2)]),
      lastName: new FormControl("", [Validators.minLength(2)]),
      birthday: new FormControl(new Date("0000/00/00").getDate(), []),
      sex: new FormControl(0, []),
      bio: new FormControl("", [Validators.required, Validators.minLength(2)]),
      url: new FormControl("", []),
      phone: new FormControl(0, [Validators.minLength(8)]),
      location: new FormControl("", [])
    });

    let id = this.route.snapshot.params.id;
    if (id != undefined && id != "") {
      this.userService.getUserById(id).subscribe(value => {
        this.user = value["data"];
        this.projects = this.user.projects;
        this.userService.getUser().subscribe(val => {
          if (this.user == val["data"]) this.isAuth = true;
        });
        this.updateForm.setValue({
          username: this.user.username,
          email: this.user.email,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
          bio: this.user.bio,
          birthday: this.user.birthday,
          sex: this.user.sex,
          url: this.user.url,
          phone: this.user.phone,
          location: this.user.location
        });
      },
      error => {
        this.router.navigateByUrl('/')
        this.toastrService.error("404 Not Found");
      });
    } else {
      this.userService.getUser().subscribe(value => {
        this.user = value["data"];
        this.projects = this.user.projects;
        this.isAuth = true;
        this.updateForm.setValue({
          username: this.user.username,
          email: this.user.email,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
          bio: this.user.bio,
          birthday: this.user.birthday,
          sex: this.user.sex,
          url: this.user.url,
          phone: this.user.phone,
          location: this.user.location
        });
      });
    }

  }

  ngOnInit() {
    let id = this.route.snapshot.params.id;
    if (id != undefined && id != "") {
      this.userService.getUserById(id).subscribe(value => {
        this.user = value["data"];
        this.projects = this.user.projects;
        this.userService.getUser().subscribe(val => {
          if (this.user == val["data"]) this.isAuth = true;
        });
      });
    } else {
      this.userNotifyChangeService.usreAnnounced.subscribe(res => {
        if (res)
          this.userService.getUser().subscribe(value => {
            this.user = value["data"];
            this.projects = this.user.projects;
            this.isAuth = true;
          });
      });
    }
  }
  public onUpdateChoice() {
    this.update = !this.update;
  }

  public updateUser() {
    let data = this.updateForm.value;
    let newUser = new User(data.username, data.email);
    newUser.firstName = data.firstName;
    newUser.lastName = data.lastName;
    newUser.birthday = data.birthday;
    newUser.sex = data.sex;
    newUser.phone = data.phone;
    newUser.location = data.location;
    newUser.url = data.url;
    newUser.bio = data.bio;
    this.userService.update(newUser).subscribe(
      res => {
        this.userNotifyChangeService.announceChange();
        this.toastrService.success("Profile updated successfully", "Profile");
      },
      err => console.log(err)
    );
  }

  public onUpdatePhotoChoice() {
    this.updatePhoto = !this.updatePhoto;
  }

  onSelectedFile(event) {
    this.selectedFiles = event.target.files[0];
    this.uploadPhoto();
  }

  uploadPhoto() {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles;

    this.userService.uploadPhotoProfile(this.currentFileUpload).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((100 * event.loaded) / event.total);
        } else if (event instanceof HttpResponse) {
          this.timeStamp = Date.now();
        }

        this.userNotifyChangeService.announceChange();
      },
      error => {
        this.toastrService.error(JSON.parse(error.error).message);
      }
    );
  }

  public onSelectedView(id: number) {
    if (id == 1) {
      this.selectedView = false;
      this.projects = this.user.projects;
    }
    else {
      this.selectedView = true;
      this.projects = this.user.contributions;
    }
  }


}
