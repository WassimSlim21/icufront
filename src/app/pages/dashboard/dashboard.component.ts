import { Component, OnInit, Input, HostListener } from '@angular/core';
// import Chart from 'chart.js';
import { ProjectService } from 'src/app/core/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AddProjectComponent } from '../add-project/add-project.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public projects;
  private links;
  private tags;
  addProject:boolean=false;
  private showNoDataFound:Boolean=false;
  private page = 0;
  tagsList: any[] = [];
  filters: string[];
  constructor(public dialog: MatDialog,private projectService: ProjectService, private router: Router, private route: ActivatedRoute) {}

  private getProjects(choice) {
    this.projectService.getAllProjects(choice)
      .subscribe(data => {
        this.projects = data['data'];
        this.links = data['meta'];
        // console.log(data);
      }, (error) => {
        console.log(error);
      }
      );

  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddProjectComponent, {
      panelClass : 'class-add'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }



  ngOnInit() {

    this.updateProject(this.tagsList, 1, 'false');
  }

  updateProject(tags: string[], page, fromPagination: string) {
    if (fromPagination === 'false') {
      this.projects = [];
      this.page = 1;
    } else if (fromPagination === 'filter') {
      this.page = 1;
      this.projects = [];
      this.filters = tags;
    }
    this.tagsList = [];
    this.tags = tags;
    // console.log(tags);
    Object.entries(tags).forEach(entry => {
      const key = entry[0];
      const value: any = entry[1];
      if (key === 'levels' || key === 'platform') {
        this.tagsList = [...this.tagsList, ...value];
      } else if (value) {

        this.tagsList.push(key);
      }
    });

    this.tagsList = this.tagsList.filter(tag => tag != null);
    // console.log('tag list : ', this.tagsList);

    this.projectService.getProjectsByTag(this.tagsList, page).subscribe(data => {
      this.projects = [...this.projects, ...data['data']];
      this.links=data['meta'];
      this.showNoDataFound=this.projects.length==0;
      console.log(this.links)

    });


  }

  // onPaginateChange(event) {
  //   this.updateProject(this.tags, (event.pageIndex + 1 ));
  // }

  @HostListener("window:scroll", [])
  onScroll(): void {
    if (((window.innerHeight + window.scrollY) >= document.body.offsetHeight) && (this.page <= this.links.last_page)) {
      console.log('=============')
      // increment the pagination
      this.page = this.page + 1;
      if (this.filters) {
        this.tagsList = this.filters;
      }
      this.updateProject(this.tagsList, this.page, 'true');
      return;
    }
  }
}
