import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/core/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})

export class ProjectsListComponent implements OnInit {

  projects:any;
  //archive project '1' or unarchive project '2' or deleted from favorites '3' or drafts '4'
  cardOperationType:number;

  constructor(private projectService:ProjectService,private router:Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    const url:String=this.router.url;

    switch(url){
      case "/projects":
        this.projectService.getUserProject().subscribe(
            projects=>{this.projects=projects.data;
              }
            ,err=>{console.log(err)}
          );
          this.cardOperationType=1;
        break;
      case "/archivedProjects":
          this.projectService.getUserArchivedProject().subscribe(
            projects=>{this.projects=projects.data;
              }
            ,err=>{console.log(err)}
          );
          this.cardOperationType=2;
          break;
      case "/favoritesProjects":
          this.projectService.getUserFavoritesProject().subscribe(
            projects=>{this.projects=projects.data;
              }
            ,err=>{console.log(err)}
          );
          this.cardOperationType=3;
          break;
      case "/drafProjects":
            this.projectService.getUserDraftProject().subscribe(
              projects=>{this.projects=projects.data;
              }
              ,err=>{console.log(err)}
            );
            this.cardOperationType=4;
            break;
    }


  }

  removeArchivedUnArchivedProject(index:number){
    this.projects.splice(index,1);
  }


}
