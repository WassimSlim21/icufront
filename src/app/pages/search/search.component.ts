import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UserService } from 'src/app/core/services/user.service';
import { ProjectService } from 'src/app/core/services/project.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {

  users: {};
  projects: {};
  searchForm: FormGroup;
  searchValue:string="";
  showProj:Boolean=false;
  showUser:Boolean=false;

  constructor(private us: UserService, private ps: ProjectService, private toast: ToastrService, private fb: FormBuilder) {

    this.searchForm=fb.group({

      searchInput:['',]

    })

   }

  ngOnInit() {
    this.searchForm = this.fb.group({
      searchInput: ['']
    })

    this.us.getAll().subscribe((res => {
      console.log(res);
      this.users = res.data;
    }),
      (err) => {
        console.log(err);
      });

    this.ps.getAllProject().subscribe((res)=>{

        this.projects=res.data;
        console.log("projects",this.projects);


    },
    (err)=>{
      console.log(err);

    })

    this.searchForm.controls.searchInput.valueChanges.subscribe(value=>this.search(value))


  }




  search(text: string) {

    if (text.length!=0){

          this.us.searchUser(text.trim()).pipe(debounceTime(700)).subscribe((res => {
            console.log("users : ", res);

            this.users = res.data;

          }),
            (err) => {
              console.log(err);
            });
          this.ps.searchProject(text.trim()).pipe(debounceTime(700)).subscribe(((res:any) => {

          // this.toast.success("yo");
            this.projects = res.data;

          }),
            (err) => {
              console.log(err);
            });

    }

  }




  projDisplay()
  {

    this.showProj=true;
    this.showUser=false;

  }
  userDisplay()
  {
    this.showUser=true;
    this.showProj=false;

  }

  DisplayAll()
  {
    this.showUser=false;
    this.showProj=false;
  }


}
