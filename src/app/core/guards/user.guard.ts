import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements  CanActivate {


  user=null;

  constructor(private router :Router,private userService:UserService,private tosterService:ToastrService){

  }


async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):Promise<boolean>{


   await  this.userService.getUser().toPromise().then(
    rez=>{
      this.user=rez;
    }).catch(err=>{});

      if(this.user.firstName.length == 0 || this.user.lastName.length == 0 )
      {
        this.router.navigateByUrl('/user')
        this.tosterService.error('You need to finish your profile first !!','Error');
        return false;
      }
      return true;
    }


}
