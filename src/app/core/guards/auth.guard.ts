import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthhService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate{

  token:boolean=false;

  constructor(private router :Router,private authService:AuthhService){

  }


 async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):Promise<boolean>{
      const data=await this.authService.checkToken().toPromise().then(
        res=>{
          this.token=true;
        }).catch(
        err=>{
          this.token=false;
          this.router.navigate(['/sign']);
        }
      );
      return this.token;
    }

}
