import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AddProjectComponent } from 'src/app/pages/add-project/add-project.component';
import { ToastrService } from 'ngx-toastr';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';

@Injectable({
  providedIn: 'root'
})
export class ProjectGuard implements  CanDeactivate<DashboardComponent> {

  constructor(private toastrService:ToastrService) {}


  canDeactivate(dashboardComponent:DashboardComponent):boolean {
    if(!dashboardComponent.addProject)
    {
      this.toastrService.warning("Project added to Draft")
    }

    return true;
  }

}
