export class Contribution {
  constructor(
    public projectId?: number,
    public contributionId?: number
  ) {}
}
