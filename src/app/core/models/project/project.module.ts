import { User } from 'src/app/core/models/user/user.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProjectModule {




    public constructor(
        public id?: number,
        public user?: User,
        public title?: string,
        public description?: string,
        public finished?: boolean,
        public tags?: string,
        public draft?: boolean,
        public level?: string) {
    }



}
