
export class User{

  public id?: number;
  public firstName?: string;
  public lastName?: string;
  public birthday?: Date;
  public sex?: number;
  public phone?: number;
  public location?: string;
  public url?: string;
  public bio?: string;
  public score?: number;
  public profilePicture?: string;
  constructor(
    public username?: string,
    public email?: string,
    public password?: string
  ) {}
}
