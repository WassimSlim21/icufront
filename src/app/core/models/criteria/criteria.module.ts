import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CriteriaModule {

  public id?: number;
  public sex?: String;
  public minAge?: number;
  public maxAge?: number;
  public targetUsers?: String;

  public constructor(
    public _id?: number,
    public _sex?: String,
    public _minAge?: number,
    public _maxAge?: number,
    public _targetUsers?: String
  )
  {


  }



 }


