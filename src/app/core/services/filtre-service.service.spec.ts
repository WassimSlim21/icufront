import { TestBed } from '@angular/core/testing';

import { FiltreServiceService } from './filtre-service.service';

describe('FiltreServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FiltreServiceService = TestBed.get(FiltreServiceService);
    expect(service).toBeTruthy();
  });
});
