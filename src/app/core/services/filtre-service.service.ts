import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FiltreServiceService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public getAllLevels() {

  const headers = new HttpHeaders({
    'Content-Type':  'application/json',
    // tslint:disable-next-line: max-line-length
    Authorization : `Bearer ${localStorage.getItem("token")}`
  });

    return this.http.get<any[]>(this.apiUrl + 'levels', {
      headers: headers
    });
   }

  public getAllPlatform() {
    const
    headers = new HttpHeaders({
      'Content-Type':  'application/json',
      // tslint:disable-next-line: max-line-length
      Authorization : `Bearer ${localStorage.getItem("token")}`
    });

    return this.http.get<any[]>(this.apiUrl + 'targetedPlateforms', {
      headers: headers
    });
  }
}
