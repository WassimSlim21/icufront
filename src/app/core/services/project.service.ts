import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import {
  HttpClient,
  HttpHeaders,
  HttpRequest,
  HttpEvent
} from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";
import { Contribution } from "src/app/core/models/contribution.module";
import { ProjectModule } from './../models/project/project.module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: "root"
})
export class ProjectService {
  private apiUrl: string = environment.apiUrl;

  constructor(public http: HttpClient) { }


  contribute(content, projectId): Observable<HttpEvent<{}>> {
    const formData = new FormData();
    formData.append("content", content);
    formData.append("project_id", projectId);
    formData.append("class", "contribution");
    return this.http.request(
      new HttpRequest("POST", this.apiUrl + "contribution", formData, {
        reportProgress: true,
        responseType: "text",
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem("token")}`
        })
      })
    );
  }

  chooseContribution(c: Contribution) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.post<any>(
      this.apiUrl + "contribution/choose",
      c,
      httpOptions
    );
  }


  getFavorites() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(this.apiUrl + "project/favorites", httpOptions);
  }

  public addFavorites(id) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };

    return this.http.post<any>(
      this.apiUrl + "project/addToFavorites/" + id,
      null,
      httpOptions
    );
  }

  public removeFromFavorites(id) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.delete<any>(
      this.apiUrl + "project/deleteFromFavorites/" + id,
      httpOptions
    );
  }

  setLike(id, path: string) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.post<any>(
      this.apiUrl + path + "/like/" + id,
      null,
      httpOptions
    );
  }

  setDislike(id, path: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.delete<any>(
      this.apiUrl + path + "/dislike/" + id,
      httpOptions
    );
  }

  getUserProject() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(this.apiUrl + "project", httpOptions);
  }

  getUserArchivedProject() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(
      this.apiUrl + "project/archivedProjects",
      httpOptions
    );
  }

  getUserFavoritesProject() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };

    return this.http.get<any>(
      this.apiUrl + "project/favorites",
      httpOptions
    );
  }

  getUserDraftProject() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(this.apiUrl + "project/drafts", httpOptions);
  }

  getUserContributions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(
      this.apiUrl + "user/contributions",
      httpOptions
    );
  }

  archiveProject(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.request(
      new HttpRequest("PUT", this.apiUrl + "project/archive/" + id, null,httpOptions
      )
    );
  }

  unarchiveProject(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.request(
      new HttpRequest("PUT", this.apiUrl + "project/unarchive/" + id, null, httpOptions)
    );
  }

  deleteFromFavorites(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.request(
      new HttpRequest(
        "DELETE",
        this.apiUrl + "project/deleteFromFavorites/" + id,
        null,httpOptions
      )
    );
  }

  deleteFromDraft(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.request(
      new HttpRequest("DELETE", this.apiUrl + 'project/deleteDraft/'+id, null, httpOptions)
    );
  }
  project: ProjectModule;

  getProjectById(id: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<ProjectModule>(this.apiUrl + 'project/' + id,httpOptions);
      }



    public getAllProjects(resouces) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
    };
      return this.http.get(this.apiUrl + resouces, httpOptions);
    }
    public getProjectsByTag(tags,page){

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        })
      };
      return this.http.post(this.apiUrl + 'projectsFilter/' + page, {"tags":tags},
      httpOptions);

    }

    // public getProjectsByTag(tags){

    //   console.log( {"tags":tags});
    //   return this.http.request(
    //     new HttpRequest("GET", this.apiUrl + 'projectsFilter', {"tags":tags},
    //   {
    //     headers: this.headers
    //   }));

    // }


    publishContent(p : any,a?:FormData)
    {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        })
      };
      return this.http.post<any>(this.apiUrl+"project",p,httpOptions);

    }

    publishMedia(p : FormData)
    {

     let header= new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem("token")}`
    })


      return this.http.request(
        new HttpRequest("POST", this.apiUrl + "project", p, {
          responseType: "text",
          headers:header
        })
       )

    }






  searchProject(s:String)
  {

    let header= new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem("token")}`
    })
    return this.http.get(this.apiUrl+"project/search/"+s ,{headers:header});

  }

  getAllProject()
  {
    let header=new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem("token")}`
    })
       return this.http.get<any>(this.apiUrl+"project/bestSixProjects",{headers:header})
  }

  getLevels()
  {

    let header= new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem("token")}`
      })
       return this.http.get<any>(this.apiUrl+"levels",{headers:header})

  }

  getPlatforms()
  {

    let header= new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem("token")}`
    })
       return this.http.get<any>(this.apiUrl+"targetedPlateforms",{headers:header})

  }





}
