import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { User } from "../models/user/user.module";
import { BehaviorSubject, Observable } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpRequest,
  HttpEvent
} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private apiUrl: string = environment.apiUrl;

  valideToken = false;

  constructor(public http: HttpClient) {}

  getUser() {
    const httpOptions = {
      headers: new HttpHeaders({
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(this.apiUrl + "user", httpOptions);
  }

  searchUser(s:String)
  {

    let header= new HttpHeaders({
      "Authorization": `Bearer ${localStorage.getItem("token")}`
      })
    return this.http.get<any>(this.apiUrl+"user/search/"+s,{headers:header});

  }

  getAll()
  {

    let header= new HttpHeaders({
      "Authorization": `Bearer ${localStorage.getItem("token")}`
      });

    return this.http.get<any>(this.apiUrl+"user/bestSixUsers",{headers:header});

  }


    getUserById(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.get<any>(this.apiUrl + "user/"+id, httpOptions);
  }

  uploadPhotoProfile(file): Observable<HttpEvent<{}>> {
    const formData = new FormData();
    formData.append("file", file);
    return this.http.request(
      new HttpRequest("POST", this.apiUrl + "user/updatePicture", formData, {
        reportProgress: true,
        responseType: "text",
        headers: new HttpHeaders({
          Authorization:`Bearer ${localStorage.getItem("token")}`
         })
      })
    );
  }

  update(u: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization:`Bearer ${localStorage.getItem("token")}`
      })
    };
    return this.http.post<any>(this.apiUrl + "user/update", u, httpOptions);
  }
}
