import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "../models/user/user.module";
import { JwtHelperService } from "@auth0/angular-jwt";
@Injectable({
  providedIn: "root"
})
export class AuthhService {
  apiUrl: string = environment.apiUrl;
  public jwtToken = null;

  constructor(private http: HttpClient) { }

  register(userModel: User) {
    return this.http.post<any>(this.apiUrl + "register", userModel);
  }

  emailVerfication (emailToken:String)
  {
    return this.http.get<any>(this.apiUrl+"signup/activate/"+emailToken);
  }

  socialSignIn(userToken: String, provider: String,social) {
    console.log("ddssdf")
    return this.http.post<any>("http://127.0.0.1:8000/oauth/token", {
      grant_type: "social",
      client_id: social.id,
      client_secret: social.secret,
      provider: provider,
      access_token: userToken
    });
  }

  login(user) {
    let res = this.http.post(this.apiUrl + "login", user, {
      observe: "response"
    });
    return res;
  }

  saveToken(jwt: string) {
    localStorage.setItem("token", jwt);
    let jwtHelper = new JwtHelperService();
    if (this.jwtToken == null) this.loadToken();
  }

  loadToken() {
    this.jwtToken = localStorage.getItem("token");
    return this.jwtToken;
  }

  logout() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })

    };
    return this.http.get<any>(this.apiUrl + "user/logout", httpOptions);

  }


  requestNewPassWord(mail: String) {
    return this.http.post<any>(this.apiUrl + "password/create", {
      "email": mail
    });
  }

  sendNewPassWord(psw: String, token: String) {
    return this.http.post<any>(this.apiUrl + "password/reset",
      {
        'password': psw,
        'token': token,
      }
    );
  }

  checkPasswordrequest(t: String) {
    return this.http.get<any>(this.apiUrl + "password/find/" + t);
  }

  checkToken(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem("token")
      })

    };
    return this.http.get<any>(this.apiUrl + "user/checkToken", httpOptions);
  }

  getSocialInfo(){
    return this.http.get<any>(this.apiUrl + "getSocialInfo");
  }
}
