import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { ToastrModule } from "ngx-toastr";

import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { BrowserModule } from "@angular/platform-browser";

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider
} from "angular-6-social-login";
import { AuthComponent } from "./pages/auth/auth.component";
import { RequestRestPasswaordComponent } from "./pages/auth/restore-password/request-rest-passwaord/request-rest-passwaord.component";
import { RestorePasswordComponent } from "./pages/auth/restore-password/restore-password.component";
import { SharedModule } from "./shared/shared.module";

// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(
        "637835463526-d0m5ck9nf31npsc1jgnck3vu3ldsku5f.apps.googleusercontent.com"
      )
    }
  ]);
  return config;
}



import {DragDropDirective} from './directives/drag-drop.directive'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgProgressModule, NgProgressInterceptor ,NgProgressBrowserXhr} from 'ngx-progressbar';
import { EmailVerficationComponent } from './pages/auth/email-verfication/email-verfication.component';
import { BrowserXhr, HttpModule } from '@angular/http';

@NgModule({
  imports: [
    HttpModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    SocialLoginModule,
    SharedModule,
    ToastrModule.forRoot(
      {
        timeOut: 10000,
        positionClass: 'toast-top-left',
        preventDuplicates: true,
        closeButton:true
      }
    ),
    HttpClientModule,
   NgProgressModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    DragDropDirective,
    AuthComponent,
    RestorePasswordComponent,
    RequestRestPasswaordComponent,
    EmailVerficationComponent,
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true },
    { provide: BrowserXhr, useClass: NgProgressBrowserXhr }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
