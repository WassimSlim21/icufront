import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { AuthComponent } from './pages/auth/auth.component';
import { RestorePasswordComponent } from './pages/auth/restore-password/restore-password.component';
import { RequestRestPasswaordComponent } from './pages/auth/restore-password/request-rest-passwaord/request-rest-passwaord.component';
import { NewPostComponent } from './shared/new-post/new-post.component';
import { AuthGuard } from './core/guards/auth.guard';
import { SignGuard } from './core/guards/sign.guard';
import { UserGuard } from './core/guards/user.guard';
import { EmailVerficationComponent } from './pages/auth/email-verfication/email-verfication.component';

const routes: Routes = [
  {
    path: "sign",
    component: AuthComponent,
    canActivate:[SignGuard]
  },
  { path: "auth/emailVerfication/:emailToken", component: EmailVerficationComponent },
  {
    path: "restorePassword/request",
    component: RequestRestPasswaordComponent
  },
  {
    path: "restorePassword/:passwordRestToken",
    component: RestorePasswordComponent
  },
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full"
  },
  {
    path: "",
    component: AdminLayoutComponent,
    canActivate:[AuthGuard],
    children: [
      {
        path: "",
        loadChildren:
          "./layouts/admin-layout/admin-layout.module#AdminLayoutModule"
      }
    ]
  },
  {
    path: "**",
    redirectTo: "dashboard"
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
