import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  constructor() { }

  @Input() meta;

  ngOnInit() {
  }

  @Output() onPageChange = new EventEmitter<string[]>();

  onPaginateChange(event){
    this.onPageChange.emit(event.pageIndex+1);
  }

}
