import { NgModule } from '@angular/core';
import {
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSelectModule,
  MatPaginatorModule,
  MatCheckboxModule

} from '@angular/material';

@NgModule({
  imports: [
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatToolbarModule,
    MatPaginatorModule
  ],
  exports: [
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatCheckboxModule
  ]
})
export class AppMaterialModule { }
