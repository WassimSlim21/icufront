import { Component, OnInit, Input,Output,EventEmitter} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ProjectService } from 'src/app/core/services/project.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
// tslint:disable-next-line: directive-class-suffix
export class CardsComponent implements OnInit {
  @Input() feed: any;
  @Input() index:number;
  @Input() cardOperationType: number;
  moment = moment; // to use it in the html template
  // tslint:disable-next-line: ban-types
  ProjectBackgroundImage: any;
  ProfileBackgroundImage:any;
  lazyImage='.././../../assets/img/load.gif';
  constructor(private projectService:ProjectService,private toastrService:ToastrService,private router:Router) { }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    this.ProjectBackgroundImage=this.feed.mainContribution.type === "media" ? this.feed.mainContribution.content : (this.feed.selectedContribution!=null ? this.feed.selectedContribution.content : "assets/img/no-image.jpg" );
    this.ProfileBackgroundImage = {'background-image': `url(${this.feed.user.profilePicture})`, 'background-size': 'cover'};
    // this.backgroundImage = this.backgroundImage + ' no-repeat';
  }

  archiveOrunArchive(){

      if(this.cardOperationType==2)
        this.unarchive();
      else if(this.cardOperationType==1)
        this.archive();
      else if(this.cardOperationType==3)
        this.deleteFromFavorites();
        else if(this.cardOperationType==4)
        this.deleteFromDraft();

  }

  @Output() onListChange = new EventEmitter<number>();


  archive(){

    this.projectService.archiveProject(this.feed.id).subscribe(
        res=>{
          this.toastrService.success('project archived');

          this.onListChange.emit(this.index);
        },
        err=>this.toastrService.error("project not archived")

      )
  }

  unarchive(){
    this.projectService.unarchiveProject(this.feed.id).subscribe(
      res=>{
        this.toastrService.success("project unarchived")
        this.onListChange.emit(this.index);
      },
      err=>this.toastrService.error("project not unarchived")

    )
  }

  deleteFromFavorites(){
    this.projectService.deleteFromFavorites(this.feed.id).subscribe(
      res=>{
        this.toastrService.success("project delete from favorites")
        this.onListChange.emit(this.index);
      },
      err=>this.toastrService.error("delete favorite project error !!")

    )
  }

  deleteFromDraft(){
    this.projectService.deleteFromDraft(this.feed.id).subscribe(
      res=>{
        this.toastrService.success("project delete from Draft")
        this.onListChange.emit(this.index);
      },
      err=>this.toastrService.error("delete Draft project error !!")
    )
  }

}
