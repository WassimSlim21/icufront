import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectNotifyChangeService {

  private projectAnnouncedChange = new Subject<boolean>();

  projectAnnounced = this.projectAnnouncedChange.asObservable();

  constructor() { }

  announceChange() {
    this.projectAnnouncedChange.next(true);
  }

}
