import { TestBed } from '@angular/core/testing';

import { ProjectNotifyChangeService } from './project-notify-change.service';

describe('UserNotifyChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectNotifyChangeService = TestBed.get(ProjectNotifyChangeService);
    expect(service).toBeTruthy();
  });
});
