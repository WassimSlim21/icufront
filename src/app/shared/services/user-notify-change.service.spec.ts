import { TestBed } from '@angular/core/testing';

import { UserNotifyChangeService } from './user-notify-change.service';

describe('UserNotifyChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserNotifyChangeService = TestBed.get(UserNotifyChangeService);
    expect(service).toBeTruthy();
  });
});
