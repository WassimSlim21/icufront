import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import {FormControl, FormBuilder, FormGroup, FormArray} from '@angular/forms';
import {MatSliderModule, MatSliderChange} from '@angular/material/slider';
import { FiltreServiceService } from 'src/app/core/services/filtre-service.service';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  levels = new FormControl();
  age: any;
  levelList: any[] = [];
  tags: string[] = [];
  targets = new FormControl();
  targetList: any[] = [];
  filterForm: FormGroup;
// tslint:disable-next-line: no-output-on-prefix
@Output() onListChange = new EventEmitter<string[]>();
  constructor(private fb: FormBuilder, private filtreService: FiltreServiceService) { }

  ngOnInit() {

    this.filtreService.getAllLevels()
    .subscribe(data => {
        this.levelList = data;
        console.log(data);
      }, (error) => {
        console.log(error);
      }
    );
    this.filtreService.getAllPlatform()
    .subscribe(data => {
        this.targetList = data;
        console.log(data);

      }, (error) => {
        console.log(error);
      }
    );
    this.filterForm = this.fb.group({
      levels: new FormControl(),
      platform: new FormControl(),
      finished:   false,
      unfinished: false,
      male:   false,
      female: false
    });
    this.filterForm.valueChanges.subscribe(value => {
      if ( value.finished === true && value.unfinished === true) {
        value.finished = false;
        value.unfinished = false;
      }
      if ( value.female === true && value.male === true) {
        value.female = false;
        value.male = false;
      }
      this.onListChange.emit(value);
      console.log('tags', value);

    });
  }


  onInputChange(event: MatSliderChange) {
    console.log(event.value);
  }

  formatLabel(value: number) {
      return Math.round(value);
  }

  }
