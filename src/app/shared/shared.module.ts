import { AppMaterialModule } from './material.module';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './cards/cards.component';
import { FilterComponent } from './filter/filter.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {MatCardModule} from '@angular/material/card';

import {MatSliderModule} from '@angular/material/slider';
import { PaginationComponent } from './pagination/pagination.component';
import { MatPaginatorModule, MatCheckboxModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { LevelBarComponent } from './level-bar/level-bar.component';



import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [CardsComponent, FilterComponent, PaginationComponent, LevelBarComponent],
  imports: [
    LazyLoadImageModule ,
    RouterModule,
    CommonModule,
    AppMaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatPaginatorModule,
    MatCheckboxModule,
    HttpClientModule,
    NgProgressModule,
    MatCardModule

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
  exports: [
    CardsComponent,
    FilterComponent,
    PaginationComponent,
    LevelBarComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }
  ]
})
export class SharedModule { }
