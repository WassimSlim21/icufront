import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-level-bar',
  templateUrl: './level-bar.component.html',
  styleUrls: ['./level-bar.component.scss']
})
export class LevelBarComponent implements OnInit {

  constructor(private userService:UserService) { }
  user:any;
  ngOnInit() {
    this.userService.getUser().subscribe(res=>this.user=res['data'])
  }

}
