import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { UserComponent } from "src/app/pages/user/user.component";
import { ProjectsListComponent } from "src/app/pages/projects-list/projects-list.component";
import { SharedModule } from "src/app/shared/shared.module";
import { ViewProjectComponent } from "src/app/pages/project/view-project/view-project.component";
import { ViewContributionComponent } from "src/app/pages/project/view-contribution/view-contribution.component";
import { MatPaginatorModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatStepperModule, MatSlideToggleModule, MatSelectModule, MatCheckboxModule, MatDividerModule, MatRadioModule, MatProgressSpinnerModule } from '@angular/material';
import { AddProjectComponent } from 'src/app/pages/add-project/add-project.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import {NewPostComponent} from '../../shared/new-post/new-post.component';


import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import { SearchComponent } from 'src/app/pages/search/search.component';

@NgModule({
  imports: [
    NgProgressModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    HttpClientModule,
    NgbModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatStepperModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFileUploadModule,
    MatCheckboxModule,
    MatDividerModule,
    MatRadioModule,
    HttpClientModule,
    NgProgressModule
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    ProjectsListComponent,
    ViewProjectComponent,
    ViewContributionComponent,
    AddProjectComponent,
    NewPostComponent,
    SearchComponent


  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }
  ]
})
export class AdminLayoutModule {}
