import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { UserComponent } from "../../pages/user/user.component";
import { ProjectsListComponent } from "src/app/pages/projects-list/projects-list.component";
import { ViewProjectComponent } from "src/app/pages/project/view-project/view-project.component";
import { ViewContributionComponent } from "src/app/pages/project/view-contribution/view-contribution.component";
import { AddProjectComponent } from 'src/app/pages/add-project/add-project.component';
import { UserGuard } from 'src/app/core/guards/user.guard';
import { SearchComponent } from 'src/app/pages/search/search.component';
import { ProjectGuard } from 'src/app/core/guards/project.guard';

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "search", component: SearchComponent },
  { path: "addProject", component: AddProjectComponent,canActivate:[UserGuard],canDeactivate:[ProjectGuard] },
  { path: "user", component: UserComponent },
  { path: "user/:id", component: UserComponent },
  { path: "projects", component: ProjectsListComponent },
  { path: "archivedProjects", component: ProjectsListComponent },
  { path: "favoritesProjects", component: ProjectsListComponent },
  { path: "contributions", component: ProjectsListComponent },
  { path: "drafProjects", component: ProjectsListComponent },
  { path: "project/:id", component: ViewProjectComponent },
];
